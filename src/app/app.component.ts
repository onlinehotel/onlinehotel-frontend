import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import {HomeComponent} from "./Component/home/home.component";
import {NavbarComponent} from "./Component/navbar/navbar.component";
import {NgbCarousel, NgbCarouselConfig, NgbSlide} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, HomeComponent, NavbarComponent, NgbCarousel, NgbSlide],
  templateUrl: './app.component.html',
  providers: [NgbCarouselConfig],
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'onlineHotel';


  images = [700, 533, 807, 124].map((n) => `https://picsum.photos/id/${n}/1300/200`);

	constructor(config: NgbCarouselConfig) {
		// customize default values of carousels used by this component tree
		config.interval = 10000;
		config.wrap = true;
		config.keyboard = false;
		config.pauseOnHover = false;
        config.showNavigationArrows= false;
        config.showNavigationIndicators= false;

	}




}

