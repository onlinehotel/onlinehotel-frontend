import {Component, OnInit} from '@angular/core';
import  list from "../../data/list-hotels.json"
import {HomeService} from "../../service/home.service";


@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  listHotel: Array<any> = []
  constructor(private HS: HomeService) {
  }
  ngOnInit(): void {

  }

}
