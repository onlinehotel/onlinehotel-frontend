export interface HotelModel {
    id: number;
    name: string;
    address: string;
    zipCode: number;
    city: string;
    description: string;
    restaurant: boolean;
    swimming_pool: boolean;
    elevator: boolean;

//    private BusinessHours operationHours;
}