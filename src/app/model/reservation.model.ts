import {RoomModel} from "./room.model";
import {ClientModel} from "./client.model";

export interface ReservationModel {
    id: number;
    startDate: Date;
    endDate: Date;
    roomID: Array<RoomModel>;
    clientID: ClientModel;
    reservationPrice: number;

}