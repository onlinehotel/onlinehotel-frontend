import {HotelModel} from "./hotel.model";

export interface AdminModel {
    id: number;
    firstName: string;
    lastName: string;
    registration_number: number;
    hotel_list: Array<HotelModel>;
}