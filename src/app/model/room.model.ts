import {HotelModel} from "./hotel.model";

export interface RoomModel {
    id: number;
    number_room: number;
    hotel_id: HotelModel;
    tv: boolean;
    wc: boolean;
    bathtub: boolean;
    shower: boolean;
    occupied: boolean;
    number_bed: number;
    price: number;

}