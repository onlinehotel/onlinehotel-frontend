export interface ClientModel {
    id: number;
    firstname: string;
    lastname: string;
    address: string;
    address2?: string;
    zip_code: number;
    city: string;
    username: string;
    password: string;
}