FROM node:18-alpine

WORKDIR /app

COPY ./package.json .
RUN npm install

COPY . .

ENV PORT 4200
EXPOSE $PORT

ENV PATH = $PATH:/app/node_modules/.bin
CMD [ "npm", "start" ]
